#include <iostream>
#include <ctime>
#include <random>
#include <functional>
#include <chrono>

#include "../customdef.h"


void foo()
{
	static int staticArr[3][100][100];
	int stackDynamicArr[3][100][100];

	static auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	static auto mtrand = std::bind(std::uniform_int_distribution<int>(1, 100), std::mt19937((unsigned int)seed));

	long long t[2];

	for (size_t i1 = 0; i1 < 2; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*staticArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**staticArr); ++i5)
			{
				staticArr[i1][i3][i5] = mtrand();
			}
		}
	}
	for (size_t i1 = 0; i1 < 2; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*stackDynamicArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**stackDynamicArr); ++i5)
			{
				stackDynamicArr[i1][i3][i5] = mtrand();
			}
		}
	}

	t[0] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	for (size_t i1 = 0; i1 < 100; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*staticArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**staticArr); ++i5)
			{
				int i6 = 0;
				for (size_t i7 = 0; i7 < ARRLEN(**staticArr); ++i7)
				{
					i6 += staticArr[0][i3][i7] * staticArr[1][i7][i5];
				}
				staticArr[2][i3][i5] = i6;
			}
		}
	}
	t[1] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::cout << "정적 100회: " << t[1] - t[0] << std::endl;

	t[0] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	for (size_t i1 = 0; i1 < 100; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*stackDynamicArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**stackDynamicArr); ++i5)
			{
				int i6 = 0;
				for (size_t i7 = 0; i7 < ARRLEN(**stackDynamicArr); ++i7)
				{
					i6 += stackDynamicArr[0][i3][i7] * stackDynamicArr[1][i7][i5];
				}
				stackDynamicArr[2][i3][i5] = i6;
			}
		}
	}
	t[1] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::cout << "스택-동적 100회: " << t[1] - t[0] << std::endl;

	t[0] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	for (size_t i1 = 0; i1 < 200; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*staticArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**staticArr); ++i5)
			{
				int i6 = 0;
				for (size_t i7 = 0; i7 < ARRLEN(**staticArr); ++i7)
				{
					i6 += staticArr[0][i3][i7] * staticArr[1][i7][i5];
				}
				staticArr[2][i3][i5] = i6;
			}
		}
	}
	t[1] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::cout << "정적 200회: " << t[1] - t[0] << std::endl;

	t[0] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	for (size_t i1 = 0; i1 < 200; ++i1)
	{
		for (size_t i3 = 0; i3 < ARRLEN(*stackDynamicArr); ++i3)
		{
			for (size_t i5 = 0; i5 < ARRLEN(**stackDynamicArr); ++i5)
			{
				int i6 = 0;
				for (size_t i7 = 0; i7 < ARRLEN(**stackDynamicArr); ++i7)
				{
					i6 += stackDynamicArr[0][i3][i7] * stackDynamicArr[1][i7][i5];
				}
				stackDynamicArr[2][i3][i5] = i6;
			}
		}
	}
	t[1] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::cout << "스택-동적 200회: " << t[1] - t[0] << std::endl;
}

int main()
{
	foo();
}
