#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#include <limits.h>
#include <malloc.h>

#define ARRLEN(arr) ( sizeof(arr) / sizeof(*(arr)) )
#define ARRLAST(arr) ( arr[ARRLEN(arr) - 1] )
#define INCMOD(idx, len) ( ( (idx) + 1 ) % (len) )
#define DECMOD(idx, len) ( ( (idx) + (len) - 1 ) % (len) )
#define INCMOD_ASSIGN(idx, len) ( (idx) = INCMOD((idx), (len)) )
#define DECMOD_ASSIGN(idx, len) ( (idx) = DECMOD((idx), (len)) )

#define BITSIZEOF(x) ( sizeof(x) * CHAR_BIT )

#define MALLOC_ASSIGN(ptr, len) \
( (ptr) = malloc(sizeof(*(ptr)) * (size_t)(len)) )
#define REALLOC_ASSIGN(dst, src, len) \
( (dst) = realloc((src), sizeof(*(dst)) * (size_t)(len)) )
#define FREE_ASSIGN(ptr) ( free(ptr), (ptr) = NULL )

#define TYPEDEF_STRUCT(name) \
typedef struct name name;\
struct name
#define TYPEDEF_ENUM(name) \
typedef enum name name;\
enum name
#define TYPEDEF_UNION(name) \
typedef union name name;\
union name

#endif
